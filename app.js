// Importing the dependencies/modules
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const taskRoute = require('./routes/taskRoutes')

// Initialize dotevn
dotenv.config();

// Server setup
const app = express();
const port = process.env.PORT || 3003;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MongoDB connection
mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0.qk2mc8i.mongodb.net/s36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', () => console.error("Connection error!"))
db.on('open', () => console.log("Connected to MongoDB"))

app.use('/tasks', taskRoute)


// Server Listening
app.listen(port, () => console.log(`Server running on localhost:${port}`));


