const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController');
// Create single task
router.post('/create', (request,response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
});

// get all task

router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result)
	})
} )

router.patch('/:id/update', (request,response) =>{
	TaskController.updateTask(request.params.id, request.body).then(result => {
		response.send(result)
	})
})

// Delete task
router.delete('/:id/delete', (request,response) => {
	TaskController.deleteTask(request.params.id).then(result => {
		response.send(result)
	})
})

// Get Specific Task
router.get('/:id', (request,response) => {
	TaskController.retrieveTask(request.params.id).then(result => {
		response.send(result)
	})
})

// change Task Status
router.patch('/:id/complete', (request,response) => {
	TaskController.completeTask(request.params.id).then(result => {
		response.send(result)
	})
})

module.exports = router;
