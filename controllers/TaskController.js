const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) =>{
		if(error){
			console.log(error);
			return error;
		};

		return savedTask

	});
};


module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask;
		})
	})
}


module.exports.deleteTask = (task_id) => {
		return Task.findByIdAndDelete(task_id).then(deletedTask => {

			if(!deletedTask) {
				return `Task not found`
			}
			
			return `Deleted \"${deletedTask.name}\" from your tasks`

		})
}

module.exports.retrieveTask = (task_id) => {
	return Task.findById(task_id).then(result => {
		if(!result){
			return `Cannot find id:${task_id}`
		}
		return result
	})
}

module.exports.completeTask = (task_id) => {
	return Task.updateOne({_id: task_id}, {status: "Completed"}).then(result => {
		if(!result){
			console.log(error);
			return `Cannot find id:${task_id}`
		}

		return `Successfully update id:${task_id}`
	})
}
